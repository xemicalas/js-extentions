// ==UserScript==
// @name         YouTube
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       You
// @match        https://www.youtube.com/*
// @grant        none
// @require      http://code.jquery.com/jquery-2.1.0.min.js
// ==/UserScript==

(function() {
    'use strict';

    //Script which:
    //Shows day's/week's total videos time
    //Ads button which downloads video's mp3
    //Ads button which searches for videos thumbnail images
    (function () {

        var btnTemplate = `
<paper-button style="margin:auto;font-size:11px;"></paper-button>
`;

        var createImageSearchButton = function () {
            var $buttonElement = $($.parseHTML(btnTemplate));
            $buttonElement.attr('id', 'search-image-component');
            $buttonElement.text('Search image');

            $buttonElement.on('click', () => {
                var imageUrl = $('meta[property="og:image"').attr('content');
                var destinationUrl = 'https://www.google.com/searchbyimage?site=search&sa=X&image_url=' + imageUrl;
                window.open(destinationUrl, '_blank');
            });

            $('#menu-container #top-level-buttons').append($buttonElement)
        };

        var createConvertToMp3Button = function () {
            var $buttonElement = $($.parseHTML(btnTemplate));
            $buttonElement.attr('id', 'convert-to-mp3-component');
            $buttonElement.text('Download MP3');

            $buttonElement.on('click', () => {
                var currentUrl = window.location.toString();
                var videoId = document.location.search.match(/v=[a-z0-9-_]+/gi)[0].replace("v=","");
                var destinationUrl = `https://xgm.lt/?download=${videoId}`;
                window.open(destinationUrl, '_blank');
            });

            $('#menu-container #top-level-buttons').append($buttonElement)
        };

        const checkForDialog = () => {
            const videoPausedSelector = $(':contains("Video paused.")');
            if (videoPausedSelector.length > 0) {
                console.log('detected video paused dialog');
                videoPausedSelector.last()
                    .closest("#main")
                    .find("#confirm-button a")
                    .first()
                    .click();

                setTimeout(() => {
                    $('button.ytp-play-button').click();
                }, 500);
            }
        };

        setInterval(() => {
            if ($('#search-image-component').length == 0) {
                createImageSearchButton();
            }
            if ($('#convert-to-mp3-component').length == 0) {
                createConvertToMp3Button();
            }
            checkForDialog();
        }, 1000);
    })();
})();