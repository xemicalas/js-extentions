// ==UserScript==
// @name         Humble Bundle
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.humblebundle.com/store/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=humblebundle.com
// @grant        none
// @require      http://code.jquery.com/jquery-2.1.0.min.js
// ==/UserScript==

(function() {
    'use strict';

    $(document).ready(function () {

        setInterval(function () {
            var productIds = JSON.parse(localStorage.getItem('excluded-products')) || {};

            $('.js-wishlist-button-container').each(function (i, el) {
                if (!el.hasAttribute('ext-loaded')) {
                    $(el).attr('ext-loaded', '')

                    var productId = $(el).closest('li.entity-block-container.js-entity-container').attr('data-entity-key');
                    if (productIds[productId]) {
                        $(el).find('.wishlist-button.js-wishlist-button').addClass('saved');
                        $(el).closest('.entity-meta').css({'background-color': 'rgba(255, 0, 0, 0.8)'});
                    }

                    el.replaceWith(el.cloneNode(true));
                }
            });

            if (!$('ul.entities-list')[0].hasAttribute('ext-loaded')) {
                $('ul.entities-list').attr('ext-loaded', '');
                $('.wishlist-button.js-wishlist-button').click(function(event) {
                    var productId = $(this).closest('li.entity-block-container.js-entity-container').attr('data-entity-key');
                    var productIds = JSON.parse(localStorage.getItem('excluded-products')) || {};
                    if (!productIds[productId]) {
                        productIds[productId] = {};
                        $(this).closest('.entity-meta').css({'background-color': 'rgba(255, 0, 0, 0.8)'});
                        $(this).addClass('saved');
                    } else {
                        $(this).removeClass('saved');
                        $(this).closest('.entity-meta').css({'background-color': 'rgba(255, 255, 255, 1)'});
                        delete productIds[productId];
                    }
                    localStorage.setItem('excluded-products', JSON.stringify(productIds));
                    event.preventDefault();
                });
            }
        }, 500);
    });
})();