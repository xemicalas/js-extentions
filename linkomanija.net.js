// ==UserScript==
// @name         Linkomanija
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       You
// @match        https://www.linkomanija.net/userdetails.php?id=326646*
// @require      http://code.jquery.com/jquery-2.1.0.min.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    let totalSize = 0;
    let totalUploaded = 0;

    const getBytes = (sizeString) => {
        let multiplier = getMultiplier(sizeString);
        let parsedValue = parseFloat(sizeString);

        return parsedValue * multiplier;
    };

    const getMultiplier = (sizeString) => {
        let multiplier = 1;
        if (sizeString.includes('KB')) {
            multiplier = 1000;
        } else if (sizeString.includes('MB')) {
            multiplier = 1000 * 1000;
        } else if (sizeString.includes('GB')) {
            multiplier = 1000 * 1000 * 1000;
        } else if (sizeString.includes('TB')) {
            multiplier = 1000 * 1000 * 1000 * 1000;
        }
        return multiplier;
    };

    const calculateRating = (size, uploaded) => {
        var rating = uploaded / size;
        return rating.toFixed(3);
    };

    const convertToGb = (bytes) => {
        let gygabytes = (bytes / (1000 * 1000 * 1000)).toFixed(2);
        return `${gygabytes} GB`;
    };

    $('td.embedded table.main tr').each((i, el) => {
        if (i > 0) {
            let sizeString = $($(el).find('td')[2]).text();
            let uploadedString = $($(el).find('td')[5]).text();

            totalSize += getBytes(sizeString);
            totalUploaded += getBytes(uploadedString);
        }
    });

    $('td.embedded table.main').append(
	$(`<tr>
		<td class="colhead"/>
		<td class="colhead"/>
		<td class="colhead">${convertToGb(totalSize)}</td>
		<td class="colhead"/>
		<td class="colhead"/>
		<td class="colhead">${convertToGb(totalUploaded)}</td>
		<td class="colhead"/>
		<td class="colhead">${calculateRating(totalSize, totalUploaded)}</td>
	</tr>`));
})();