// ==UserScript==
// @name         Dark Mode
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Dark themes > light themes, enjoy!
// @author       You
// @match        https://trakt.tv/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=trakt.tv
// @grant        GM_addStyle
// ==/UserScript==

var darkMode=`
section:not(.user-wrapper), section[id*='content-page'], .pagination a, .popover, .popover .metadata-wrapper,
.popover-title, .popover-content ul.lists li, select, .poster-under, .summary-activity, .dropdown-menu, textarea, input, .tooltip.actor-tooltip *, #seasons-episodes-sortable div.titles {
color: white !important;
background-color: #1d1d1d !important;
}

#schedule-wrapper, #network-wrapper, #standard-features, #recent-wrapper, section.subnav-wrapper, #progress-wrapper .seasons, .comment-wrapper, .text-wrapper .under-count, .season-links, #list-modal, #checkin-modal, .panel-body, .table tbody>tr, h3.instructions, .overview blockquote {
color: white !important;
background: none !important;
background-color: #161616 !important;
}

#bottom-stats-wrapper{
background-color: black !important;
}

.summary-activity .tab.selected, .users-wrapper {
background-color: #333333 !important;
border: none !important;
color: white !important;
}

.panel-heading {
background-color: #333333 !important;
color: white !important;
}

.panel, .panel-heading {
border: 1px solid #333333 !important;
}

.summary-activity .tab {
border: 1px solid #333333 !important;
color: #d1d1d1 !important;
opacity: 1 !important;
}

.summary-activity .tab * {
opacity: 1 !important;
}

.btn-watch, .btn-collect, .btn-list, .btn-recommendations, .btn-comment {
background-color: #222222;
}

#info-wrapper .sidebar .poster, #info-wrapper .sidebar .streaming-links, .checkin-modal {
border-color: black!important;
box-shadow: 0 0 20px 0 black !important;
}

#info-wrapper .sidebar .btn-watch-now {
box-shadow: 0 5px 20px 0 black !important;
}

.poster-item {
box-shadow: 0 0 15px black !important;
}

.poster, .quick-icons {
border: none !important;
}

h3, h4, a.titles-link, a.username, #ondeck-shows .reset:hover, #ondeck-shows .ignore:hover, .sections a:hover, .above-comment .date,
.season-links a.selected, .dropdown-menu>li>a, #seasons-episodes-sortable .under-info h3 a {
color: white !important;
}

#schedule-wrapper .schedule-episode:not(:first-of-type) {
border-top-color: #575757 !important;
}

.popover-title, h3.instructions {
border-bottom-color: #575757 !important;
}

.activity-date {
color: white !important;
background-color: #272727 !important;
}

.pagination>.active>a {
background-color: #ed1c24 !important;
}

.pagination a:hover, #seasons-episodes-sortable .under-info h3 a:hover, h4:has(.fa-heart) {
color: #ed1c24 !important;
}

.dropdown button, .external a, .affiliate-links a, .btn-group .btn {
background-color: #444 !important;
color: white !important;
border: none !important;
}

.dropdown button:hover, .external a:hover, .affiliate-links a:hover, .dropdown-menu>li>a:hover {
background-color: #303030 !important;
}

.fade-watching-collecting button, button.dropdown-toggle {
background-color: inherit !important;
}

.summary-comments .above-comment, #seasons-episodes-sortable .titles {
background-color: #444 !important;
}

.summary-comments .comment, .summary-comments .under-comment, #seasons-episodes-sortable .titles .episode-stats {
background-color: #292929 !important;
}

.quick-icons {
background-color: #121212 !important;
}

.under-comment .interactions {
margin-top: 0px !important;
}

#seasons-episodes-sortable ul.additional-stats h4.premiere {
color: #08a508 !important;
}

.shade {
background-image: -webkit-linear-gradient(top,rgba(0,0,0,0) 0%,black 100%) !important;
background-image: -o-linear-gradient(top,rgba(0,0,0,0) 0%,black 100%) !important;
background-image: linear-gradient(to bottom,rgba(0,0,0,0) 0%,black 100%) !important;
}
`
GM_addStyle(darkMode);