// ==UserScript==
// @name         XGM
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       You
// @match        https://xgm.lt/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    if (document.location.search.indexOf("?download=") > -1) {
        var videoId = document.location.search.replace("?download=","");

        $.ajax({
            url: 'http://localhost:21223/v1/audio/download',
            type: 'PUT',
            datatype: 'json',
            data: JSON.stringify({ AudioId: videoId }),
            crossDomain: true,
            xhrFields: { withCredentials: true },
            headers: {
                "Content-Type": "application/json"
            },
        }).always(() => {
            window.close();
        });
    }
})();