// ==UserScript==
// @name         Hellcase
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       You
// @match        https://*.hellcase.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    let balance = parseFloat($('.balance:first').text().trim());

    setTimeout(() => {

        if (balance >= 1.00 && window.location.pathname.indexOf('/open/bounty') <= -1) {
            window.location.pathname = '/en/open/bounty';
        }

        if (window.location.pathname.indexOf('/open/bounty') > -1) {
            if (balance >= 1.00) {
                $('#btnOpen')[0].click();
            }
        }

        if (window.location.pathname.indexOf('/profile/') > -1) {
            if ($('#my-items .item.usercase').length > 0) {
                $('#my-items .item.usercase:first .get a').click();
            } else {
                $('#my-items .item .send').each((i, el) => {
                    el.click();
                });
                setTimeout(() => {
                    if ($('#withdraw_agree .container_checkbox:visible').length > 0) {
                        $('#withdraw_agree .container_checkbox').click();
                        $('#withdraw_agree .bottom a')[0].click();
                    }
                }, 1000);
            }
        }

        if (window.location.pathname.indexOf('/dailyfree') > -1) {
            $('#btn_open_daily_free').click();

            let previousBalance = balance;
            $('.history_table tr').each((i, el) => {
                let balanceType = $(el).children().eq(1).text();
                if (balanceType.includes("Balance")) {
                    let bonusAmount = parseFloat($(el).children().eq(2).text().replace(/\+ /g, ''));
                    $(el).append(`<td>=<i class="core-dollar"></i>${previousBalance}</td>`);
                    previousBalance = Math.round((previousBalance - bonusAmount) * 100) / 100;
                }
            });
        }

        if (window.location.pathname.indexOf('/profile/') > -1) {
            setTimeout(() => {
                window.location.pathname = '/en/dailyfree';
            }, 5000);
        }

        if (window.location.pathname.indexOf('/dailyfree') > -1) {
            setTimeout(() => {
                $('li.user > a:first')[0].click();
                window.location.reload();
            }, 120000);
        } else {
            setTimeout(() => {
                window.location.pathname = '/en/dailyfree';
            }, 120000);
        }
    }, 2000);
})();