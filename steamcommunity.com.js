// ==UserScript==
// @name         Steam
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       You
// @match        *://steamcommunity.com/*
// @match        *://store.steampowered.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    class NotificationsManager {

        constructor() {
            this._handleKeydownEvents();
        }

        render() {
            this._renderBotNotification();
        }

        _handleKeydownEvents() {
            jQuery(document).keydown(evt => {
                if (evt.which === 16) {
                    let toggle = localStorage.getItem('disableb') || 0;
                    localStorage.setItem('disableb', !JSON.parse(toggle));
                    this.render();
                    if (!isDisabled()) {
                        toMainPage();
                    }
                }
            });
        }

        _renderBotNotification() {
            if (jQuery('.client-notification').length === 0) {
                this._createBotNotification();
            } else {
                this._updateBotNotification();
            }
        }

        _createBotNotification() {
            let $botElement = jQuery('<div></div>')
            .text(this._getBotText())
            .addClass('client-notification')
            .css('color', 'red')
            .css('position', 'fixed')
            .css('left', '10px')
            .css('bottom', '10px');
            jQuery('body').append($botElement);
        }

        _updateBotNotification() {
            jQuery('.client-notification').text(this._getBotText());
        }

        _getBotText() {
            return `Client: ${!isDisabled()} (SHIFT to toggle)`;
        }
    }

    let isDisabled = () => JSON.parse(localStorage.getItem('disableb'));

    let isTradeOffered = () => jQuery('.header_notification_tradeoffers')
    .hasClass('active_inbox_item');

    let isGiveaway = () => {
        let yourItemsCount = jQuery("#trade_yours .trade_slot.has_item").length;
        let treirItemsCount = jQuery("#their_slots .trade_slot.has_item").length;

        return yourItemsCount === 0 && treirItemsCount > 0;
    }

    let toMainPage = () => {
        window.history.replaceState({}, document.title, '.');
        window.location.pathname = '';
    }

    setTimeout(() => {

        if (window.location.href.indexOf('account/registerkey') > -1) {
            document.getElementById("accept_ssa").checked = true;
            document.getElementById("register_btn").click();
        }

        //start going to the queue
        if (jQuery('.discovery_queue_static').is(':visible')) {
            jQuery('.discover_queue_empty_refresh_btn a')[0].click();
        }

        //go though queue
        let elements = jQuery('.next_in_queue_content');
        if (elements.length > 0) {
            elements[0].click();
        }

        //skip age confirmation in the queue
        if (jQuery('.age_queue_btn_ctn').length > 0) {
            jQuery('.btn_next_in_queue_trigger')[0].click();
        }

        //go to the next queue
        if (jQuery('.discovery_queue_apps:contains("You can get")').length > 0) {
            jQuery('#refresh_queue_btn').click();
        }

        if (window.location.pathname.indexOf('/openid/login') > -1) {
            jQuery('#imageLogin')[0].click();
        }

        if (window.location.pathname == '/' && !isDisabled()) {
            setInterval(() => {
                if (isTradeOffered() && !isDisabled()) {
                    let tradeOffersUrl = jQuery('.header_notification_tradeoffers').attr('href');
                    window.location = tradeOffersUrl;
                }
            }, 3000);
        }

        if (window.location.pathname.indexOf('/tradeoffers/') > -1 && !isDisabled()) {
            let tradeOfferEl = jQuery('.tradeoffer .tradeoffer_items_ctn.active:first');
            if (tradeOfferEl.length > 0) {
                let tradeOfferId = tradeOfferEl.parent().attr('id').replace(/[^0-9]/gi, '');
                window.location = `https://steamcommunity.com/tradeoffer/${tradeOfferId}/`;
            }
        }

        if (window.location.pathname.indexOf('/tradeoffer/') > -1 && !isDisabled()) {
            if (isGiveaway()) {
                setTimeout(() => {
                    jQuery("#you_notready")[0].click();
                    setTimeout(() => {
                        if (jQuery('.btn_green_white_innerfade:visible').length > 0) {
                            jQuery('.btn_green_white_innerfade:visible').each((i, el) => {
                                el.click();
                            });
                            setTimeout(() => {
                                jQuery("#trade_confirmbtn")[0].click();
                            }, 1000);
                        } else {
                            jQuery("#trade_confirmbtn")[0].click();
                        }
                    }, 1000);
                }, 4000);
            }
        }
    }, 2000);

    setTimeout(() => {
        if (!isDisabled()) {
            toMainPage();
        }
    }, 30000);

    new NotificationsManager().render();
})();