// ==UserScript==
// @name         Indiegala
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       You
// @match        https://www.indiegala.com/*
// @exclude      https://www.indiegala.com/giveaways/detail/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    let points = null;
    if (location.pathname.indexOf('/giveaways') > -1) {

        var waitUntilVisible = function (el, callback) {
            var interval = setInterval(() => {
                var isVisible = $(el).is(':visible');
                if (isVisible) {
                    callback();
                    clearInterval(interval);
                }
            }, 1000);
        };

        var waitUntilGiveawaysVisible = function (callback) {
            waitUntilVisible($('.page-contents-ajax'), callback);
        };

        var joinGiveaways = function () {
            points = null;
            let delay = 0;
            $('.page-contents-list-cont .items-list-row .items-list-col').each((i, el) => {
                var isAvailable = $(el).find('.items-list-item-ticket').length > 0;
                var isOwner = $(el).find('.items-list-item-owner').length > 0;
                if (isAvailable && !isOwner) {
                    var giveawayPoints = parseInt($(el).find('.items-list-item-data-button a').attr('data-price'));
                    var ticketId = $(el).find('h5 a').attr('href').replace(/.*\//, '');
                    setTimeout(() => {
                        if (points == null || points >= giveawayPoints) {
                            joinGiveaway(el, ticketId);
                        }
                    }, delay);
                    delay += 4000;
                }
            });
        };

        var joinGiveaway = function (container, ticketId) {
            $.post(
                '/giveaways/join',
                JSON.stringify({id: ticketId}),
                (data) => {
                    let parsedData = JSON.parse(data);
                    points = parsedData.silver_tot || 0;
                    if (parsedData.status && parsedData.status === "duplicate") {
                        points = null;
                    }
                    if (parsedData.status === "ok") {
                        $(container).find('.items-list-item-ticket').remove();
                    }
                }
            );
        };

        var drawPoints = function () {
            let $pointsElement = $('<div></div>')
            .text(`Points: ${getPoints()}`)
            .addClass('client-notification')
            .css('position', 'fixed')
            .css('left', '10px')
            .css('bottom', '20px')
            .css('color', 'red');

            if ($('.client-notification').length === 0) {
                $('body').append($pointsElement);
            } else {
                $('.client-notification').text(`Points: ${getPoints()}`);
            }
        };

        var getPoints = function () {
            return points || 0;
        };

        var getCurrentPage = function () {
            var page = parseInt($('.pagination .current').text());
            return Number.isNaN(page) || !page ? null : page;
        };

        var getCurrentPageLevel = function () {
            var level = parseInt($('.page-contents-list-submenu-current-level span').text().replace(/[^0-9.]/g, ''));
            return Number.isNaN(level) || !level ? null : level;
        };

        var setNextLevel = function(level) {
            window.setLevel(level, this, new Event('click'));
        };

        var getNextPagePath = function (level, page) {
            return `/giveaways/ajax/${page}/expiry/asc/level/${level}`;
        };

        var getTotalPages = function () {
            var path = $('.pagination .page-link-cont .prev-next:last').attr('href');
            if (!path) {
                return 0;
            }
            var result = /giveaways\/ajax\/(\d+)/gi.exec(path);
            return result.length >= 2 ? parseInt(result[1]) : 0;
        }

        var paginate = function () {
            var currentPage = getCurrentPage();
            var currentLevel = getCurrentPageLevel();
            var nextPageNumber = currentPage + 1;
            var nextLevel = currentLevel;
            var totalPages = getTotalPages();

            if (currentPage >= totalPages) {
                nextPageNumber = 1;
                if (++nextLevel > 5) {
                    nextLevel = 2;
                }
            }

            if (currentPage >= 10) {
                nextPageNumber = 1;
            }

            var nextPage = getNextPagePath(nextLevel, nextPageNumber);
            if (nextLevel != currentLevel) {
                setNextLevel(nextLevel);
            } else {
                window.loadGiveawaysListContents(nextPage);
            }

            waitUntilGiveawaysVisible(joinGiveaways);
        };

        waitUntilGiveawaysVisible(() => {
            var currentPage = getCurrentPage();
            var currentLevel = getCurrentPageLevel();

            if (currentPage == null || currentLevel == null) {
                setNextLevel(5);
                waitUntilGiveawaysVisible(joinGiveaways);
            }
        });


        setInterval(() => {
            drawPoints();
        }, 1000);

        setInterval(() => {
            paginate();
        }, 60000);
    }

    if (location.pathname.indexOf('/library') > -1) {
        $('#new-giveaway-auction .new-tradegiv-description').val('Good luck :)');

        setInterval(() => {
            if ($('#check-all-button').length <= 0) {
                $('#giveaways-completed-tocheck .profile-private-sub-section-header.library-giveaways-check-all-to-hide .profile-private-sub-section-row').append(
                    '<div class="profile-private-sub-section-col-20 profile-private-sub-section-col" id="check-all-button">'+
                    '<div class="profile-private-sub-section-col-inner align-c">'+
                    '<a class="library-giveaways-check-all-btn" href="#" onclick="giveawayCheckIfWinnerAll(this, event)">Check all</a>'+
                    '</div></div>');
            }
        }, 1000);

    }

    $(document).on('click','.btn-open-leave-feedback-form', () => {
        $('.feedback-text').val('thanks');
    });

    setInterval(() => {
        if (document.hasFocus()) {
            $('.serial-won').off('click').on('click', function() {
                $(this).find('input').select();
                var serialKey = $(this).find('input').val();
                var steamUrl = 'https://store.steampowered.com/account/registerkey?key=';
                window.open(steamUrl + serialKey, '_blank');
            });
        }
    }, 1000);
})();