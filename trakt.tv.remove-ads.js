// ==UserScript==
// @name         Disable Ads
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://trakt.tv/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=trakt.tv
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    let interval;
    let iteration = 0;
    const removeAds = () => {
        if (iteration >= 20) {
            clearInterval(interval);
            return;
        }
        $('div').each((i, el) => {
            let text = $(el).text();
            let isAd = text.toLowerCase() === 'advertisement' && $(el).children().length == 0;

            if (isAd) {
                let rootElement = $(el).closest('div[id$="wrapper"]');
                if (rootElement.length > 0) {
                    rootElement.remove();
                } else {
                    $(el).parent().remove();
                }
            }
        });
        iteration++;
    };

    interval = setInterval(removeAds, 100);
})();