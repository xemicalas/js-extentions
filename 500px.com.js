setInterval(() => {
    
    $('.activity_item').each((i, el) => {
        var imgUrl = $(el).find('.photo_activity_item__img').attr('src');
    
        if ($(el).find('.img-url').length <= 0) {
            var $element = $('<a/>')
            .addClass('img-url')
            .attr('href', imgUrl)
            .attr('target', '_blank')
            .text(imgUrl);
            
            $(el).find('.photo_activity_item__top')
                .after($element);
        } else {
            $(el).find('.img-url')
                .attr('href', imgUrl)
                .text(imgUrl);
        }
    });
    
    if ($('.main_container').length > 0) {
        var imgUrl = $('.main_container').find('.photo').attr('src');
        var $targetContainer = $('.main_container').find('.img-url');
        if ($targetContainer.length <= 0
            || !$targetContainer.attr('href')) {
            var $element = $('<a/>')
            .addClass('img-url')
            .attr('href', imgUrl)
            .attr('target', '_blank')
            .text(imgUrl);
            
            $targetContainer.empty();
            $('.main_container').append($element);
        } else {
            $targetContainer.attr('href', imgUrl)
                .text(imgUrl);
        }
    }
    

}, 1000);