// ==UserScript==
// @name         Wallhaven wallpaper
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       You
// @match        https://w.wallhaven.cc/*
// @grant        none
// @require      http://code.jquery.com/jquery-2.1.0.min.js
// ==/UserScript==

(function() {
    'use strict';

    if ($('h1:contains("503 Service Temporarily Unavailable")').length > 0) {
        setTimeout(() => {
            window.location.reload();
        }, 1000);
    }
})();