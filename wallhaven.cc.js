// ==UserScript==
// @name         Wallhaven
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       You
// @match        https://wallhaven.cc/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const storagekey = 'visitedWallpapers';
    const ttlThreshold = 60*60*24*28; // four weeks

    let getCurrentTimestamp = () => Math.floor(new Date().getTime() / 1000);

    let isTtlExeeded = timestamp =>
    getCurrentTimestamp() - ttlThreshold > timestamp;

    let revokeOldWallpapers = wallpapers => {
        for (var wallpaper in wallpapers) {
            if (wallpapers.hasOwnProperty(wallpaper)) {
                let wallpaperTimestamp = wallpapers[wallpaper];
                if (isTtlExeeded(wallpaperTimestamp)) {
                    delete wallpapers[wallpaper];
                }
            }
        }
        saveVisitedWallpapers(wallpapers);
    };

    let loadVisitedWallpapers = () =>
    JSON.parse(localStorage.getItem(storagekey));

    let saveVisitedWallpapers = visitedWallpapers =>
    localStorage.setItem(storagekey, JSON.stringify(visitedWallpapers));

    let getWallpaperId = $el => $el.parent().attr('data-wallpaper-id');

    let storeWallpaperId = (wallpapers, $el) => {
        let wallpaperId = getWallpaperId($el);
        wallpapers[wallpaperId] = getCurrentTimestamp();
        saveVisitedWallpapers(wallpapers);
    };

    let setVisited = $el => $el.css('background-color', 'green')
    .css('opacity', '0.2');

    let replaceWallpaperTargetUrl = (wallpaperId, $el) => {
        var extension = 'jpg';
        if ($el.next().find('.png').length > 0) {
            extension = 'png';
        }
        let wallpaperPrefix = wallpaperId.substring(0, 2);
        let newUrl = `https://w.wallhaven.cc/full/${wallpaperPrefix}/wallhaven-${wallpaperId}.${extension}`;
        $el.attr('href', newUrl);
    };

    let processWallpaper = (wallpapers, $el) => {
        let wallpaperId = getWallpaperId($el);
        if (wallpapers[wallpaperId]) {
            setVisited($el);
        }
        replaceWallpaperTargetUrl(wallpaperId, $el);
    };

    let observeChanges = wallpapers => {
        let observer = new MutationObserver(mutationList => {
            for (let mutation of mutationList) {
                for (let node of mutation.addedNodes) {
                    if (node.className === 'thumb-listing-page') {
                        $(node).find('a.preview').each((i, el) => {
                            processWallpaper(wallpapers, $(el));
                        });
                    }
                }
            }
        });
        observer.observe($('#thumbs')[0], { childList: true });
    };

    $(document).ready(() => {
        let wallpapers = loadVisitedWallpapers() || {};
        revokeOldWallpapers(wallpapers);

        $('a.preview').each((i, el) => {
            processWallpaper(wallpapers, $(el));
        });

        $(document).on('click', 'a.preview', function () {
            storeWallpaperId(wallpapers, $(this));
            setVisited($(this));
        });

        $(document).on('mousedown', 'a.preview', function (e) {
            if (e.which === 2) {
                storeWallpaperId(wallpapers, $(e.target));
                setVisited($(e.target));
            }
        });

        observeChanges(wallpapers);
    });
})();