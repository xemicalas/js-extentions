// ==UserScript==
// @name         SteamGifts
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       You
// @match        https://www.steamgifts.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=steamgifts.com
// @grant        none
// ==/UserScript==

(function() {
  'use strict';

  const username = 'AXEMYA';

  class NotificationsManager {
    render() {
      this._renderBotNotification();
    }

    _renderBotNotification() {
      if ($('.client-notification').length === 0) {
        this._createBotNotification();
      } else {
        this._updateBotNotification();
      }
    }

    _createBotNotification() {
      let $botElement = $('<div></div>')
        .text(this._getBotText())
        .addClass('client-notification')
        .css('position', 'fixed')
        .css('left', '10px')
        .css('bottom', '10px');
      $('body').append($botElement);
    }

    _updateBotNotification() {
      $('.client-notification').text(this._getBotText());
    }

    _getBotText() {
      return `Client: ${!Bot.isDisabled()} (SHIFT to toggle)`;
    }
  }

  class Bot {

    constructor() {
      this._coreHandler = new CoreHandler();
      this._notificationsManager = new NotificationsManager();
    }

    run() {
        this._handleKeydownEvents();
        this._notificationsManager.render();
      if (!Bot.isDisabled()) {
        this._startCountdownToRefresh();
        this._handleWonEvent();
        this._coreHandler.handle();
      }
    }

    _handleWonEvent() {
      if (this._hasWon()) {
        if (!this.isDisabledAlert()) {
          this._disableAlertNotification();
          this._showWinningsAlert();
        }
      } else {
        this._enableAlertNotification();
      }
    }

    _handleKeydownEvents() {
      $(document).keydown(evt => {
        if ($('input, textarea').is(':focus') || window.getSelection().toString().length > 0) {
          return;
        }
        if (evt.which === 16) {
          let toggle = localStorage.getItem('disableb') || 0;
          localStorage.setItem('disableb', !JSON.parse(toggle));
          this._notificationsManager.render();
          if (!Bot.isDisabled()) {
            Bot.toMainPage();
          }
        }
      });
    }

    static getCurrentPoints() {
      return parseInt($('.nav__points').first().text());
    }

    static getCurrentPath() {
      return window.location.pathname;
    }

    static toMainPage() {
      window.location = location.protocol + "//" + location.hostname;
    }

    isDisabledAlert() {
      return JSON.parse(localStorage.getItem('disable'));
    }

    static isDisabled() {
      return JSON.parse(localStorage.getItem('disableb'));
    }

    static getCurrentTimestamp() {
        return Math.round(new Date().getTime() / 1000);
    }

    _startCountdownToRefresh() {
      setInterval(() => {
        Bot.toMainPage();
      }, 60000);
    }

    _hasWon() {
      return $('.fa.fa-trophy').last().siblings().length > 0;
    }

    _disableAlertNotification() {
      localStorage.setItem('disable', true);
    }

    _enableAlertNotification() {
      localStorage.setItem('disable', false);
    }

    _showWinningsAlert() {
      alert('WON!!!');
    }
  }

  class CoreHandler {

    handle() {
      if (Bot.getCurrentPath() === '/') {
        new GiveawaysWindowHandler().handle();
        } else {
        new GiveawayWindowHandler().handle();
      }
    }
  }

  class GiveawaysWindowHandler {

    constructor() {
      this._giveawaysAnalyzer = new GiveawaysAnalyzer();
    }

    handle() {
      this._unhideSpecialGiveaways();
      if (Bot.getCurrentPoints() >= 30) {
        this._giveawaysAnalyzer.analyze();
      }
    }

    _unhideSpecialGiveaways() {
      $('.pinned-giveaways__inner-wrap')
        .removeClass('pinned-giveaways__inner-wrap--minimized')
        .addClass('pinned-giveaways__inner-wrap');
    }
  }

  class GiveawayWindowHandler {

    handle() {
      setTimeout(() => {
        Bot.toMainPage();
      }, 30000);

      this._writeRandomComment();

      let sidebar = $('.sidebar__entry-insert');
      let deleteBtn = $('.sidebar__entry-delete');
      let refreshInterval = setInterval(() => {
        let btnHidden = $(sidebar).hasClass('is-hidden');
        let deleteBtnHidden = $(deleteBtn).hasClass('is-hidden');
        if (btnHidden && !deleteBtnHidden) {
          clearInterval(refreshInterval);
          Bot.toMainPage();
        }
        if (!btnHidden) {
          $(sidebar).click();
        }
      }, 100);
    }

    _writeRandomComment() {
      let giveawayCommentator = new CurrentGiveawayCommentator();
      giveawayCommentator.writeRandomComment();
    }
  }

  class GiveawaysAnalyzer {

    constructor() {
      this._giveawaysReader = new GiveawaysReader();
    }

    analyze() {
      let processedGiveaways = this.getProcessedGiveaways();
      if (processedGiveaways.length > 0) {
        this._openGiveaway(processedGiveaways[0]);
      }
    }

    getProcessedGiveaways() {
      let giveaways = this._giveawaysReader.read();
      let filteredGiveaways = giveaways.filter(x => x.canJoin);
      return GiveawaysSorter.sortByScore(filteredGiveaways);
    }

    _openGiveaway(giveaway) {
      window.location = location.protocol + "//"
        + location.hostname + giveaway.url;
    }
  }

  class GiveawaysSorter {

    static sortByScore(giveaways) {
      giveaways.sort((a, b) => b.score - a.score);
      return giveaways;
    }
  }

  class GiveawaysReader {
    read() {
      let giveaways = [];
      $('.giveaway__row-outer-wrap').each((i, el) => {
        let giveawayReader = new GiveawayReader(el);
        let giveaway = giveawayReader.read();
        giveaways.push(giveaway);
      });
      return giveaways;
    }
  }

  class GiveawayReader {

    constructor(el) {
      this._el = el;
    }

    read() {
      let entries = this._readEntries();
      let copies = this._readCopies();
      let contributorLevel = this._readContributorLevel();
      let timestamp = this._readTimestamp();
      let score = this._calculateScore(
          entries, copies, contributorLevel, timestamp);

      return {
        url: this._readUrl(),
        score: score,
        canJoin: this._canJoin()
      };
    }

    _canJoin() {
      let isGiveawayFaded = this._isGiveawayFaded();
      let isOwnedGiveaway = this._readGiveawayUsername() === username;
      let giveawayPoints = this._readGiveawayPoints();

      return !isGiveawayFaded && !isOwnedGiveaway && Bot.getCurrentPoints() >= giveawayPoints;
    }

    _isGiveawayFaded() {
      return $(this._el).find('.giveaway__row-inner-wrap')
        .attr('class').match(/ is-faded/gi);
    }

    _readGiveawayUsername() {
      return $(this._el).find('.giveaway_image_avatar').attr('href').replace('/user/', '');
    }

    _readGiveawayPoints() {
      return parseInt($(this._el).find('.giveaway__heading__thin').last()
        .text().replace(/[^0-9]/gi, ''));
    }

    _readEntries() {
      return parseInt($(this._el).find('.giveaway__links')
        .find('span').first().text().replace(/[^0-9]/gi, ''));
    }

    _readCopies() {
      let copies = 1;
      let giveawayHeadingText = $(this._el).find('.giveaway__heading__thin').first().text();
      if (giveawayHeadingText.match(/\d+ copies/gi)) {
        copies = giveawayHeadingText.replace(/[^0-9]/gi, '');
      }
      return copies;
    }

    _readContributorLevel() {
      return parseInt($(this._el)
        .find('.giveaway__column--contributor-level--positive')
        .text().replace(/[^0-9]/gi, '')) || 0;
    }

    _readTimestamp() {
        return parseInt($(this._el)
            .find('.giveaway__columns > div:first > span:first')
            .attr('data-timestamp'));
    }

    _readUrl() {
      return $(this._el).find('.giveaway__heading__name')
        .attr('href');
    }

    _calculateScore(entries, copies, contributorLevel, timestamp) {
      let timeDifference = timestamp - Bot.getCurrentTimestamp();
      let entriesFactor = copies / entries;
      let timeFactor = entriesFactor * (10 / timeDifference);
      let contributorLevelFactor = entriesFactor * contributorLevel;
      return entriesFactor + timeFactor + contributorLevelFactor;
    }
  }

  class CurrentGiveawayCommentator {
    constructor() {
      this.randomThankWords = [
        'Thank you',
        'Thank youu',
        'Thank yoouu',
        'Thank yoouuu',
        'Thank you so much',
        'Thank youu so much',
        'Thank you soo much',
        'Thank you sooo much',
        'Thank you very much',
        'Thx',
        'Thanks',
        'Thanks so much'
      ];

      this.randomComments = [
        '',
        ', marked as received',
        ', great game',
        ', awesome game',
        ', the key was valid',
        ' for this gift',
        ' for the gift',
        ' for the giveaway',
        ' for this giveaway',
        ' for the game',
      ];

      this.randomSmiles = [
        '',
        ':)',
        ';)',
        '^^',
        '^_^',
        ':]',
        ';]'
      ];

      this.randomMarks = [
        '',
        '.',
        '!',
        '!!',
        '!!!',
        '!!!!',
        '!!!!!',
      ]
    }

    writeRandomComment() {
      let textAreaEl = this._getElement();
      let randomComment = this._getRandomComment();
      textAreaEl.text(randomComment);
    }

    writeComment(comment) {
      let textAreaEl = this._getElement();
      textAreaEl.text(comment);
    }

    _getElement() {
      return $('.comment.comment--submit form [name=description]');
    }

    _getRandomComment(texts) {
      let randomThankWordIndex = Math.floor(Math.random() * this.randomThankWords.length);
      let randomCommentIndex = Math.floor(Math.random() * this.randomComments.length);
      let randomSmileIndex = Math.floor(Math.random() * this.randomSmiles.length);
      let randomMarkIndex = Math.floor(Math.random() * this.randomMarks.length);
      return `${this.randomThankWords[randomThankWordIndex]}${this.randomComments[randomCommentIndex]}${this.randomMarks[randomMarkIndex]} ${this.randomSmiles[randomSmileIndex]}`;
    }
  }

  new Bot().run();
})();